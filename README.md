# Phalcon Migrations Sandbox

Throwing sand from one place to another to build better sand castle.

## Results [v2.1.1]

| Description           | Size  | Tables | Rows      | Generate Time | Run Time    | Notes |
| --------------------- | ----- | ------ | --------- | ------------- | ----------- | ----- |
| Insert many rows      | 80Mb  |   1    | 1.000.000 | 7.485s        | 6m 24.172s  |       |
| Insert + alter column | 80Mb  |   1    | 1.000.000 | 7.368s        | 37m 16.080s | It seems that alter is executed after every single insert |


## Results [v2.1.2]

| Description           | Size  | Tables | Rows      | Generate Time | Run Time    | Notes |
| --------------------- | ----- | ------ | --------- | ------------- | ----------- | ----- |
| Insert many rows      | 80Mb  |   1    | 1.000.000 | 7.392s        | 26.736s (x14 improvement)     |       |
| Insert + alter column | 80Mb  |   1    | 1.000.000 | 7.186s        | 28.922s     |       |
